package sample;
import com.hp.ucmdb.api.UcmdbService;
import com.hp.ucmdb.api.topology.*;
import com.hp.ucmdb.api.types.CI;
public class DataInSample {
    static final String CIT_NAME_HOST ="node";
    static final String ATTRIBUTE_NAME_HOST_NAME ="name";
    static final String CIT_NAME_IP = "ip_address";
    static final String ATTRIBUTE_NAME_IP_DOMAIN ="routing_domain";
    static final String ATTRIBUTE_NAME_IP_ADDRESS_SHORT ="name";
    static final String CIT_NAME_HOST_IP_LINK = "containment";
    static final String ATTRIBUTE_NAME_HOST_SERIAL_NUMBER = "serial_number";

    public static void main(String[] args) throws Exception{
        addNodeWithIPSample();
        deleteNodeByReferencedIpSample();
        mergeNodesSample();
    }
    /**
     * In this flow we create node, ip and a link between the two, and add them to the Ucmdb.
     */
    public static void addNodeWithIPSample() throws Exception{
        final String name = "testNode";
        final String domain = "Default Domain";
        final String address = "1.1.1.10";
        final TopologyUpdateService topologyUpdateService = getUcmdbService().getTopologyUpdateService();
        final TopologyUpdateFactory factory = topologyUpdateService.getFactory();
        final TopologyModificationData addData = factory.createTopologyModificationData();
        // Create a CI of type node.
        final CI addedNode = addData.addCI(CIT_NAME_HOST);
        // Add name property to the node.
        addedNode.setStringProperty(ATTRIBUTE_NAME_HOST_NAME, name);
        // Create a CI of type ip.
        final CI addedIp = addData.addCI(CIT_NAME_IP);
        // Add domain property to the ip.
        addedIp.setStringProperty(ATTRIBUTE_NAME_IP_DOMAIN, domain);
        // Add address property to the ip.
        addedIp.setStringProperty(ATTRIBUTE_NAME_IP_ADDRESS_SHORT, address);
        // Create a link between the node and the ip.
        addData.addRelation(CIT_NAME_HOST_IP_LINK, addedNode, addedIp);
        // Add the data to the UCMDB, in case that the object exists - it will be updated
        topologyUpdateService.create(addData, CreateMode.UPDATE_EXISTING);
        // We print the node ID from the UCMDB.
        System.out.println("The added node ID is : " + addedNode.getId());
    }
    /**
     * In this flow we create node, ip and a link between the two, and add them to the Ucmdb.
     * After they were added successfully we will remove the node (and the link) from the Ucmdb,
     * we will recognize the node using the ip as reference instead of his name.
     * In the end we will also remove the ip.
     */
    public static void deleteNodeByReferencedIpSample() throws Exception{
        final String name = "testNode";
        final String domain = "Default Domain";
        final String address = "1.1.1.10";
  final TopologyUpdateService topologyUpdateService = getUcmdbService().getTopologyUpdateService();
        final TopologyUpdateFactory factory = topologyUpdateService.getFactory();
        final TopologyModificationData addData = factory.createTopologyModificationData();
        // Create a CI of type node.
        final CI addedNode = addData.addCI(CIT_NAME_HOST);
        // Add name property to the node.
        addedNode.setStringProperty(ATTRIBUTE_NAME_HOST_NAME, name);
        // Create a CI of type ip.
        final CI addedIp = addData.addCI(CIT_NAME_IP);
        // Add domain property to the ip.
        addedIp.setStringProperty(ATTRIBUTE_NAME_IP_DOMAIN, domain);
        // Add address property to the ip.
        addedIp.setStringProperty(ATTRIBUTE_NAME_IP_ADDRESS_SHORT, address);
        // Create a link between the node and the ip.
        addData.addRelation(CIT_NAME_HOST_IP_LINK, addedNode, addedIp);
        // Add the data to the Ucmdb
        topologyUpdateService.create(addData, CreateMode.UPDATE_EXISTING);

        final TopologyModificationData deleteNodeByReferencedIp = factory.createTopologyModificationData();
        // We delete the node (and the link) without specify his name, but by using the ip as reference.
        final CI deletedNode = deleteNodeByReferencedIp.addCI(CIT_NAME_HOST);
        final CI deletedIpReference = deleteNodeByReferencedIp.addReferenceCI(CIT_NAME_IP);
        deletedIpReference.setStringProperty(ATTRIBUTE_NAME_IP_DOMAIN, domain);
        deletedIpReference.setStringProperty(ATTRIBUTE_NAME_IP_ADDRESS_SHORT, address);
        deleteNodeByReferencedIp.addRelation(CIT_NAME_HOST_IP_LINK, deletedNode, deletedIpReference);
        // Delete the node and the link. The ip remain in the Ucmdb.
        topologyUpdateService.delete(deleteNodeByReferencedIp, DeleteMode.FAIL_IF_NOT_EXIST);
        final TopologyModificationData deleteIp = factory.createTopologyModificationData();
        // We will delete the remained ip by using his real Ucmdb id.
        final CI deletedIp = deleteIp.addCI(addedIp.getId(), CIT_NAME_IP);
        // Delete the ip from the Ucmdb.
        topologyUpdateService.delete(deleteIp, DeleteMode.FAIL_IF_NOT_EXIST);
    }
    /**
     * In this flow we create two nodes, one of them have name and the other one have serial number, and add them to the Ucmdb.
     * After they were added successfully we will add a new node with the same name and serial number, and it will force
     * the two nodes to merge.
     */
    public static void mergeNodesSample() throws Exception{
     final String name = "nodeName";
        final String sn = "SN";
        final TopologyUpdateService topologyUpdateService = getUcmdbService().getTopologyUpdateService() ;
        final TopologyUpdateFactory factory = topologyUpdateService.getFactory();
        final TopologyModificationData addData = factory.createTopologyModificationData();
        // Create two CIs ofs type node.
        final CI addedNodeWithName = addData.addCI(CIT_NAME_HOST);
        final CI addedNodeWithSN = addData.addCI(CIT_NAME_HOST);
        // Add name property to first node.
        addedNodeWithName.setStringProperty(ATTRIBUTE_NAME_HOST_NAME, name);
        // Add serial number property to second node.
        addedNodeWithSN.setStringProperty(ATTRIBUTE_NAME_HOST_SERIAL_NUMBER, sn);
        // Add the data to the Ucmdb.
        topologyUpdateService.create(addData, CreateMode.UPDATE_EXISTING);
        final TopologyModificationData updateData = factory.createTopologyModificationData();
        // We add a new node with both name and serial number.
        final CI mixedNode = updateData.addCI(CIT_NAME_HOST);
        mixedNode.setStringProperty(ATTRIBUTE_NAME_HOST_NAME, name);
        mixedNode.setStringProperty(ATTRIBUTE_NAME_HOST_SERIAL_NUMBER, sn);
        // Update the Ucmdb data.
        topologyUpdateService.update(updateData);

        final TopologyModificationData deleteData = factory.createTopologyModificationData();
        // We will clean the Ucmdb.
        final CI deletedNode = deleteData.addCI(CIT_NAME_HOST);
        deletedNode.setStringProperty(ATTRIBUTE_NAME_HOST_NAME, name);
        deletedNode.setStringProperty(ATTRIBUTE_NAME_HOST_SERIAL_NUMBER, sn);
        // Delete the data from the Ucmdb.
        topologyUpdateService.delete(deleteData, DeleteMode.IGNORE_NON_EXISTING);
    }
    /**
     * Getting a connection
     */
    public static UcmdbService getUcmdbService() throws Exception {
        return CreateSDKConnectionSample.createSDKConnection();
    }
}
