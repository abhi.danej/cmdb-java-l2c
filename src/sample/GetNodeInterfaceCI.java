package sample;
import com.hp.ucmdb.api.UcmdbService;
import com.hp.ucmdb.api.topology.*;
import com.hp.ucmdb.api.types.TopologyCI;
import com.hp.ucmdb.api.types.TopologyRelation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
/**
 * This is a sample of executing an AdHoc query.
 * This sample creates a query for searching nodes with at least two IPs.
 * Then, the sample prints the results.
 */
public class GetNodeInterfaceCI {
    public static void main(String[] args) throws Exception{
        // Create a connection
    	System.out.println("Creating connection..");
        UcmdbService  ucmdbService = CreateSDKConnectionSample.createSDKConnection();
        // Getting the topology service
        TopologyQueryService queryService = ucmdbService.getTopologyQueryService();
        // Get the query factory
        TopologyQueryFactory queryFactory = queryService.getFactory();
        // Create the query definition
        System.out.println("Creating query..");
        QueryDefinition queryDefinition = queryFactory.createQueryDefinition("Get nodes with more than one network interface");
        //The unique name of the query node of type "node"
        
        ArrayList<String> nodeList = new ArrayList<>();
        nodeList.add("zimbra");
        nodeList.add("awscollector");
        
        String nodeName = "Node";
        // Creating a query node from type “node" and asking for all returned nodes the display_label attribute
        QueryNode nodeQuery = queryDefinition.addNode(nodeName).ofType("node").
        		queryProperty("display_label").
        		property("display_label",Operator.EQUALS_CASE_INSENSITIVE , "zimbra");
        QueryNode intQuery = queryDefinition.addNode("interface Node").ofType("interface").queryProperty("display_label");
        // Link the node to ip_address with link of type contains and define the minimal link cardinality of the node to be 2
        nodeQuery.linkedTo(intQuery).withLinkOfType("composition");

        System.out.println("Executing query:");
        Topology topology = queryService.executeQuery(queryDefinition);

        Collection<TopologyCI> nodes = topology.getCIsByName(nodeName);
        // Go over the nodes and print its related IPs
        System.out.println("Printing results:");
        for (TopologyCI nodeCI : nodes) {
            System.out.print("Node " + nodeCI.getPropertyValue("display_label") +"  ");
            for (TopologyRelation relation : nodeCI.getOutgoingRelations()) {
                System.out.print(relation.getEnd2CI().getPropertyValue("display_label")+"  ");
            }
            // Break line
            System.out.print("\n");
        }
    }
}
