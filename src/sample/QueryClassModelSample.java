package sample;
 import com.hp.ucmdb.api.UcmdbService;
 import com.hp.ucmdb.api.classmodel.Attribute;
 import com.hp.ucmdb.api.classmodel.ClassDefinition;
 import com.hp.ucmdb.api.classmodel.ClassModelService;
 import java.util.Collection;
 import java.util.Map;
 /**
  * Class Model Query Sample
  * This sample queries for a specific CMDB class.
  * Output:
  *  - The display name and description of the class
  *  - List of all the declared attributes of the class
  *  - List of the directly derived classes
  */
 public class QueryClassModelSample {
  public static void main(String[] args) throws Exception {
   UcmdbService service = CreateSDKConnectionSample.createSDKConnection();
   // Getting the class model service
   ClassModelService classModel = service.getClassModelService();
   // Class to be query
   String classToQuery = "layer2_connection";
   // Getting the attributes of a class
   ClassDefinition definition = classModel.getClassDefinition(classToQuery);
   // Print the display nme of the class
   System.out.println("Class label is: "+definition.getDisplayName());
   System.out.println("Description: " + definition.getDescription()+"\n");
   // Get the the declared attributes
   Map<String,Attribute> attributes = definition.getDeclaredAttributes();
   // Print the declare attributes
   System.out.println("Attributes (declare on the specific class, without the derived ones):");
   System.out.println("-------------------------------------------------------------------");
   // Go over the attributes and print them
   for (Attribute attribute : attributes.values()) {
    System.out.println(attribute.getName()+"["+attribute.getDisplayLabel()+"]");
   }
   System.out.println("\nDirectly derived Classes:");
   System.out.println("------------------------");
   // Get derived classes (directly derived, not all derived)
   Collection<ClassDefinition> children = definition.getChildClasses();
   // Go over classes and print them
   for (ClassDefinition child : children) {
    System.out.println(child.getName()+"["+child.getDisplayName()+"]");
   }
  }
 }