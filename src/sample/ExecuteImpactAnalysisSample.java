package sample;
import com.hp.ucmdb.api.UcmdbService;
import com.hp.ucmdb.api.impact.*;
import com.hp.ucmdb.api.topology.*;
import com.hp.ucmdb.api.types.CI;
/**
 * Created by IntelliJ IDEA.
 * User: sharir
 * Date: Jan 12, 2011
 * Time: 12:27:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExecuteImpactAnalysisSample {
    public static void main(String[] args) throws Exception{
        // Create a connection
        UcmdbService ucmdbService = CreateSDKConnectionSample.createSDKConnection();
        // Getting the Impact Analysis service
        ImpactAnalysisService impactAnalysisService = ucmdbService.getImpactAnalysisService();
        ImpactAnalysisFactory impactFactory = impactAnalysisService.getFactory();
        ImpactAnalysisDefinition definition = impactFactory.createImpactAnalysisDefinition();
        // Random a CPU CI to be a trigger CI
        CI ci = getRandomCPU(ucmdbService);
        // Define the trigger ci and the severity
        definition.addTriggerCI(ci).withSeverity(impactFactory.getSeverityByName("Critical"));
        // Execute impact analysis
        ImpactAnalysisResult impactResult = impactAnalysisService.analyze(definition);
        // Get Affected CIs
        AffectedTopology affectedCIs = impactResult.getAffectedCIs();
        // Go over the affected CIs and print them
        for (AffectedCI affectedCI : affectedCIs.getAllCIs()) {
            System.out.println("Affected " + affectedCI.getType() + " " + affectedCI.getId() + " - severity " + affectedCI.getSeverity());
        }
    }
     /**
       Get a random CPU CI
    */
    private static CI getRandomCPU(UcmdbService service){
        // Getting the topology service
        TopologyQueryService queryService = service.getTopologyQueryService();
        // Get the query factory
        TopologyQueryFactory queryFactory = queryService.getFactory();
        // Create the query definition
        QueryDefinition queryDefinition = queryFactory.createQueryDefinition("Get All CPUs");
        // Creating a query node from type “cpu�? and asking for the display_label attribute
        QueryNode node = queryDefinition.addNode("cpu").ofType("cpu");
        // Make executable
        ExecutableQuery executableQuery = queryDefinition.toExecutable();
        // Set chunk (page) size to 1 - since we want a single CI
        executableQuery.setMaxChunkSize(1);
        // Execute the query
        Topology results = queryService.executeQuery(executableQuery);
        // Get the first CPU
        return results.getAllCIs().iterator().next();
    }
}

 
