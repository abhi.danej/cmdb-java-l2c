package sample;

import com.hp.ucmdb.api.UcmdbService;
import com.hp.ucmdb.api.topology.CreateMode;
import com.hp.ucmdb.api.topology.TopologyModificationData;
import com.hp.ucmdb.api.topology.TopologyUpdateFactory;
import com.hp.ucmdb.api.topology.TopologyUpdateService;
import com.hp.ucmdb.api.types.CI;

public class LinkInterfaces {
	
    static final String CIT_NAME_HOST ="node";
    static final String CIT_NAME_L2C ="layer2_connection";
    static final String CIT_NAME_INT ="interface";
    
    static final String ATTRIBUTE_NAME_HOST_NAME ="name";
    static final String CIT_NAME_IP = "ip_address";
    static final String ATTRIBUTE_NAME_IP_DOMAIN ="routing_domain";
    static final String ATTRIBUTE_NAME_IP_ADDRESS_SHORT ="name";
    static final String CIT_NAME_HOST_IP_LINK = "containment";
    static final String ATTRIBUTE_NAME_HOST_SERIAL_NUMBER = "serial_number";

	public static void main(String[] args) throws Exception {
		linkIntefaces();
	}
	
    public static UcmdbService getUcmdbService() throws Exception {
        return CreateSDKConnectionSample.createSDKConnection();
    }

	private static void linkIntefaces() throws Exception {
		
        final String name = "zimbra-awscollecotr";
        final String domain = "Default Domain";
        final String address = "1.1.1.10";
        final TopologyUpdateService topologyUpdateService = getUcmdbService().getTopologyUpdateService();
        final TopologyUpdateFactory factory = topologyUpdateService.getFactory();
        final TopologyModificationData addData = factory.createTopologyModificationData();
        
        CI l2c = addData.addCI(CIT_NAME_L2C);
        l2c.setStringProperty("name", name);
        l2c.setStringProperty("display_label", name);
        
        GetInterfaceCI cmi = new GetInterfaceCI();
        System.out.println("Getting zimbra if");
        CI if1 = cmi.getInterface("zimbra", "ens160");
        addData.addCI(if1);
        
        System.out.println("Getting aws if");
        CI if2 = cmi.getInterface("awscollector", "ens160");
		addData.addCI(if2); 
        
        addData.addRelation("membership", l2c, if1);
        addData.addRelation("membership", l2c, if2);
        
        System.out.println("Updating Topology...");
        topologyUpdateService.create(addData, CreateMode.UPDATE_EXISTING);
        System.out.println("The added node ID is : " + l2c.getId());
        System.out.println("Program Completed...");
        /*
        // Create a CI of type node.
        final CI addedNode = addData.addCI(CIT_NAME_HOST);
        // Add name property to the node.
        addedNode.setStringProperty(ATTRIBUTE_NAME_HOST_NAME, name);
        // Create a CI of type ip.
        final CI addedIp = addData.addCI(CIT_NAME_IP);
        // Add domain property to the ip.
        addedIp.setStringProperty(ATTRIBUTE_NAME_IP_DOMAIN, domain);
        // Add address property to the ip.
        addedIp.setStringProperty(ATTRIBUTE_NAME_IP_ADDRESS_SHORT, address);
        // Create a link between the node and the ip.
        addData.addRelation(CIT_NAME_HOST_IP_LINK, addedNode, addedIp);
        // Add the data to the UCMDB, in case that the object exists - it will be updated
        topologyUpdateService.create(addData, CreateMode.UPDATE_EXISTING);
        // We print the node ID from the UCMDB.
        System.out.println("The added node ID is : " + addedNode.getId());
        */
	}

}
