package sample;
import com.hp.ucmdb.api.*;
import java.net.MalformedURLException;
/**
 * This is a sample of how to establish a connection to the UCMDB server.
 * This sample is connecting to the server according to the constants below.
 * WORKS ONLY WITH JAVA 8
 * 	OR YOU WILL GET NoClassDefFoundError for sun/misc/BASE64Encoder
 */
public class CreateSDKConnectionSample {
//	public static final String PROTOCOL ="https";
//    public static final String HOST ="serviceassurance.advantageinc.org";
//    public static final int PORT = 443;
//    private static final String USER_NAME = "admin";
//    private static final String PASSWORD = "MFS0ftware%";
    
	/**
    public static final String PROTOCOL ="https";
	public static final String HOST ="serviceconfiguration.mfdemo.net";
	//	private static final String USER_NAME = "abhishekd";
    private static final String USER_NAME = "sysadmin";
    private static final String PASSWORD = "MFS0ftware%";
    public static final int PORT = 8443;
    */
    
    // Lotus cmdb
    public static final String PROTOCOL ="https";
	public static final String HOST ="cmdb.innovationai.in";
    private static final String USER_NAME = "admin";
    private static final String PASSWORD = "Admin_123";
    public static final int PORT = 8443;
    /**
     * Creates a UCMDB SDK connection.
     *
     * @return UcmdbService object with all of the available services
     * @throws java.net.MalformedURLException in case of a wrong host/protocol/port input
     */
    public static UcmdbService createSDKConnection() throws MalformedURLException {
        //Creating a service provider for a given UCMDB server address
        UcmdbServiceProvider serviceProvider = UcmdbServiceFactory.getServiceProvider(PROTOCOL, HOST, PORT);
        //Creating a client context according to the name of this integration (for auditing)
        ClientContext clientContext = serviceProvider.createClientContext("MyAppName");
        //Creating the credentials for authentication
        Credentials credentials = serviceProvider.createCredentials(USER_NAME, PASSWORD);
        // Creating the connection object
        return serviceProvider.connect(credentials,clientContext);
     }
}