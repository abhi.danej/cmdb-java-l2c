package sample;
import com.hp.ucmdb.api.UcmdbService;
import com.hp.ucmdb.api.topology.*;
import com.hp.ucmdb.api.view.ViewService;
import com.hp.ucmdb.api.view.ViewWithFoldingDefinition;
import com.hp.ucmdb.api.view.foldingdefinition.FoldingDefinition;
import com.hp.ucmdb.api.view.foldingdefinition.FoldingDefinitionNodesFactory;
import com.hp.ucmdb.api.view.result.ViewResult;
import com.hp.ucmdb.api.view.result.ViewResultTreeNode;
import java.util.List;
/**
 *
 * This is a sample of creating and executing a view.
 * This sample:
 * - Creates a query for searching nodes with at least two IPs.
 * - Creates a view definition in order to create a folded results of the the query.
 *   The folding rule in use will be: for each Node, fold its IPs as its children.
 * - Print the results by traversing the tree result
 *
 *
 */
public class CreateAndExecuteViewSample {
    public static void main(String[] args) throws Exception{
        // Create a connection
        UcmdbService  ucmdbService = CreateSDKConnectionSample.createSDKConnection();
        // Getting the view service
        ViewService viewService = ucmdbService.getViewService();
        //create new view definition
        ViewWithFoldingDefinition newView =
                        viewService.getFactory().createViewWithFoldingDefinition("MyView");
        // Get the folding definition of the view
        FoldingDefinition definition = newView.foldingDefinition();
        // Getting a factory of view nodes
        FoldingDefinitionNodesFactory factory = definition.nodesFactory();
        // Creating the view definition:
        // - Node
        //    - Ip Address
        definition.addRoot(factory.createQueryNodeBasedFDN("Node As Parent")).withQueryNodeName("Node Query Node").
                        addChild(factory.createQueryNodeBasedFDN("Ip As Child")).withQueryNodeName("Ip Query Node");
        // Create the query which is going to be executed by the view
        ExecutableQuery query = createQueryForView(ucmdbService);
        // Execute the view
        ViewResult result = viewService.executeViewDefinition(newView, query);
        // Get the roots of the result (the top most parents)
        List<? extends ViewResultTreeNode> roots = result.roots();
        // Go over the nodes and print the amount of IPs and their addresses
        for (ViewResultTreeNode root : roots) {
            // Get the node's children (the IPs)
            List<? extends ViewResultTreeNode> children = root.children();
            // Print the Node and the IP count
            System.out.println("-"+ root.label() + " ["+ children.size()+"]");
            // Go over the IPs and print them
            for (ViewResultTreeNode child : children) {
                System.out.println("  |-"+child.label() );
            }
        }
    }
    /**
       Creates a query for searching nodes with at least two IPs
    */
    private static ExecutableQuery createQueryForView(UcmdbService service){
        // Getting the topology service
        TopologyQueryService queryService = service.getTopologyQueryService();
        // Get the query factory
        TopologyQueryFactory queryFactory = queryService.getFactory();
        // Create the query definition
        QueryDefinition queryDefinition = queryFactory.createQueryDefinition("Get nodes with more than one network interface");
        //The unique name of the query node of type "node"
        String nodeName = "Node Query Node";
        // Creating a query node from type �node? and asking for all returned nodes the display_label attribute
        QueryNode node = queryDefinition.addNode(nodeName).ofType("node").queryProperty("display_label");
        // Creating a node from type �ip_address? asking for all returned nodes the ip_address attribute
        QueryNode ipNode = queryDefinition.addNode("Ip Query Node").ofType("ip_address").queryProperty("ip_address");
        // Link the node to ip_address with link of type contains and define the minimal link cardinality of the node to be 2
        node.linkedTo(ipNode).withLinkOfType("containment").atLeast(2);
        // Make the definition executable
        return queryDefinition.toExecutable();
    }
}
 
