package sample;
import com.hp.ucmdb.api.UcmdbService;
import com.hp.ucmdb.api.topology.*;
import com.hp.ucmdb.api.types.CI;
import com.hp.ucmdb.api.types.TopologyCI;
import com.hp.ucmdb.api.types.TopologyRelation;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
/**
 * This is a sample of executing an AdHoc query.
 * This sample creates a query for searching nodes with at least two IPs.
 * Then, the sample prints the results.
 */
public class GetInterfaceCI {
    public static void main(String[] args) throws Exception{
        // Create a connection
    	GetInterfaceCI self = new GetInterfaceCI();
    	CI ifCI = self.getInterface("zimbra", "ens160");
    	System.out.println(ifCI.getPropertyValue("interface_macaddr"));

    }

	public CI getInterface(String nodeDisplayLabel, String ifDisplayLabel) throws MalformedURLException {
		System.out.println("Creating connection..");
        UcmdbService  ucmdbService = CreateSDKConnectionSample.createSDKConnection();
        // Getting the topology service
        TopologyQueryService queryService = ucmdbService.getTopologyQueryService();
        // Get the query factory
        TopologyQueryFactory queryFactory = queryService.getFactory();
        // Create the query definition
        System.out.println("Creating query..");
        QueryDefinition queryDefinition = queryFactory.createQueryDefinition("Get nodes with more than one network interface");
        //The unique name of the query node of type "node"
        
        String ifName = "Interface";

        QueryNode intQuery = queryDefinition.addNode(ifName).ofType("interface").
        		queryProperty("display_label").queryProperty("interface_macaddr").
        		property("display_label", Operator.EQUALS_CASE_INSENSITIVE, ifDisplayLabel);
        
        QueryNode nodeQuery = queryDefinition.addNode("node query").ofType("node").
        		queryProperty("display_label").
        		property("display_label",Operator.EQUALS_CASE_INSENSITIVE , nodeDisplayLabel);
//        intQuery.linkedTo(nodeQuery).withLinkOfType("composition");
        intQuery.linkedFrom(nodeQuery).withLinkOfType("composition");

        System.out.println("Executing query:");
        Topology topology = queryService.executeQuery(queryDefinition);

        Collection<TopologyCI> ciList = topology.getCIsByName(ifName);
        // Go over the nodes and print its related IPs
        System.out.println("Printing results:");
        System.out.println("Program closed.");
        return ciList.iterator().next();
	}
}
