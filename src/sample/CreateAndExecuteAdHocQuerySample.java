package sample;
import com.hp.ucmdb.api.UcmdbService;
import com.hp.ucmdb.api.topology.*;
import com.hp.ucmdb.api.types.TopologyCI;
import com.hp.ucmdb.api.types.TopologyRelation;
import java.util.Collection;
/**
 * This is a sample of executing an AdHoc query.
 * This sample creates a query for searching nodes with at least two IPs.
 * Then, the sample prints the results.
 */
public class CreateAndExecuteAdHocQuerySample {
    public static void main(String[] args) throws Exception{
        // Create a connection
    	System.out.println("Creating connection..");
        UcmdbService  ucmdbService = CreateSDKConnectionSample.createSDKConnection();
        // Getting the topology service
        TopologyQueryService queryService = ucmdbService.getTopologyQueryService();
        // Get the query factory
        TopologyQueryFactory queryFactory = queryService.getFactory();
        // Create the query definition
        System.out.println("Creating query..");
        QueryDefinition queryDefinition = queryFactory.createQueryDefinition("Get nodes with more than one network interface");
        //The unique name of the query node of type "node"
        String nodeName = "Node";
        // Creating a query node from type “node�? and asking for all returned nodes the display_label attribute
        QueryNode node = queryDefinition.addNode(nodeName).ofType("node").queryProperty("display_label");
        // Creating a node from type “ip_address�? asking for all returned nodes the ip_address attribute
        QueryNode ipNode = queryDefinition.addNode("Ip Node").ofType("ip_address").queryProperty("ip_address");
        // Link the node to ip_address with link of type contains and define the minimal link cardinality of the node to be 2
        node.linkedTo(ipNode).withLinkOfType("containment").atLeast(2);
        // Execute the unsaved query
        Topology topology = queryService.executeQuery(queryDefinition);
        // Get the node results
        Collection<TopologyCI> nodes = topology.getCIsByName(nodeName);
        // Go over the nodes and print its related IPs
        for (TopologyCI nodeCI : nodes) {
            System.out.print("Node " + nodeCI.getPropertyValue("display_label") +"  ");
            // Get related IPs from the node
            for (TopologyRelation relation : nodeCI.getOutgoingRelations()) {
                System.out.print(relation.getEnd2CI().getPropertyValue("ip_address")+"  ");
            }
            // Break line
            System.out.print("\n");
        }
    }
}
