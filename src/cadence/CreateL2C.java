package cadence;

import java.net.MalformedURLException;
import java.net.URL;

import com.hp.ucmdb.api.UcmdbException;
import com.hp.ucmdb.api.UcmdbService;
import com.hp.ucmdb.api.topology.CreateMode;
import com.hp.ucmdb.api.topology.TopologyModificationData;
import com.hp.ucmdb.api.topology.TopologyUpdateFactory;
import com.hp.ucmdb.api.topology.TopologyUpdateService;
import com.hp.ucmdb.api.types.CI;

public class CreateL2C {
	
    static final String CIT_NAME_HOST ="node";
    static final String CIT_NAME_L2C ="layer2_connection";
    static final String CIT_NAME_INT ="interface";
    static final String REL_L2C_TO_INT = "membership";
    static URL cmdbURL = null; 
    static String userName = null;
    static String password = null;
	
	public CreateL2C(String line, URL cmdbURL, String userName, String password) throws Exception {
		
		CreateL2C.cmdbURL = cmdbURL;
		CreateL2C.userName = userName;
		CreateL2C.password = password;

		try {
			
			String nodeALabel = line.split(",")[0];
			String nodeAInterfaceLabel = line.split(",")[1];
			String nodeBLabel = line.split(",")[2];
			String nodeBInterfaceLabel = line.split(",")[3];
			
			final TopologyUpdateService topologyUpdateService = getUcmdbService().getTopologyUpdateService();
			final TopologyUpdateFactory factory = topologyUpdateService.getFactory();
			final TopologyModificationData topoModData = factory.createTopologyModificationData();

			final String name = nodeALabel + nodeBLabel;
			CI l2c = topoModData.addCI(CIT_NAME_L2C);
			l2c.setStringProperty("name", name);
			l2c.setStringProperty("display_label", name);
			
			QueryInterfaceCI cmi = new QueryInterfaceCI(getUcmdbService());
			
//			System.out.println("SEARCHING  " + nodeALabel);
			CI if1 = cmi.getInterface(nodeALabel, nodeAInterfaceLabel);
			topoModData.addCI(if1);
			
//			System.out.println("Getting IF for " + nodeBLabel);
			CI if2 = cmi.getInterface(nodeBLabel, nodeBInterfaceLabel);
			topoModData.addCI(if2); 
			
			topoModData.addRelation(REL_L2C_TO_INT, l2c, if1);
			topoModData.addRelation(REL_L2C_TO_INT, l2c, if2);
			
			System.out.println("UPDATING TOPOLOGY...");
			topologyUpdateService.create(topoModData, CreateMode.UPDATE_EXISTING);
			System.out.println("CREATED/UPDATED Layer2Connectiviy CI ID: " + l2c.getId());
		} catch (MalformedURLException e) {
			System.out.println("UNABLE TO CREATE Layer 2 Connection. CONTACT ADMINISTRATOR.");
			e.printStackTrace();
		} catch (UcmdbException e) {
			System.out.println("UNABLE TO CREATE Layer 2 Connection. CONTACT ADMINISTRATOR.");
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("UNABLE TO CREATE Layer 2 Connection. CONTACT ADMINISTRATOR.");
			e.printStackTrace();
		}
	}
	
	public static UcmdbService getUcmdbService() throws Exception {
        return CreateSDKConnection.createSDKConnection(cmdbURL, userName, password);
    }

}
