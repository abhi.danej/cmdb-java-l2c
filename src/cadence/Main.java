package cadence;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.MalformedURLException;
import java.net.URL;

import com.hp.ucmdb.api.UcmdbService;

public class Main {
	

    
    static URL cmdbURL = null;
    static String userName = null;
    static String password = null;
    static FileReader fr = null;
    
	public static void main(String[] args) throws Exception {
		String url = args[0];
		
		// url, user, password, csv_file_path
		try {
			if(validateInputs(url, args[1], args[2], args[3])) {
				userName = args[1];
				password = args[2];
				cmdbURL = new URL(url);
				fr = new FileReader(args[3]);
				linkIntefaces();
			}
		} catch(Exception e) {
			System.out.println("Please validate inputs.");
			printUsage();
		}
		System.out.println("PROGRAM COMPLETED.");
	}
	
	
    private static boolean validateInputs(String url, String userName, String password, String fileName) {
    	System.out.println("VALIDATING INPUTS: " + url + "," + userName + "," + password + "," + fileName);
    	URL cmdb;
    	int port;
    	String host;

    	if(userName.isEmpty() || userName==null || password.isEmpty() || password==null || fileName.isEmpty() || fileName==null) {
    		System.out.println("Verify the inputs.");
    		printUsage();
    		return false;
    	}
    	
    	try {
			new FileReader(fileName);
		} catch (FileNotFoundException e1) {
			System.out.println("Error while reading file.");
			e1.printStackTrace();
			return false;
		}
    	
    	try {
			cmdb = new URL(url);
			host = cmdb.getHost();
			port = cmdb.getPort();
			if(port==-1) {
				System.out.println("Please specific port in URL.");
				return false;
			}
		} catch (MalformedURLException e) {
			System.out.println("Verify the URL and try again.");
			e.printStackTrace();
			return false;
		}
    	System.out.println("INPUT VALIDATION DONE.");
		return true;

	}

	private static void printUsage() {
		System.out.println("USAGE:");
		System.out.println("java -jar AppName.jar https://<cmdb_fqdn>:<port> user-name password csv-file-path");
	}


	public static UcmdbService getUcmdbService() throws Exception {
        return CreateSDKConnection.createSDKConnection(cmdbURL, userName, password);
    }

	private static void linkIntefaces() throws Exception {
		
		String line = null;
		BufferedReader br = new BufferedReader(fr);
		int lineCount = 0; 
		while( (line=br.readLine()) != null) {
			lineCount++;
			
			if(verifyLine(line)) {
				
//				createLayer2Connection(nodeALabel,nodeAInterfaceLabel,nodeBLabel,nodeBInterfaceLabel);
				CreateL2C l2c = new CreateL2C(line, cmdbURL, userName, password);
				
			} else {
				System.out.println("Line " + lineCount + " has problem, please verify. [" + line + "]");
			}

		}
		
	}


	private static boolean verifyLine(String line) {
		try {
			System.out.println("PROCESSING LINE: [" + line + "]");
			String nodeALabel = line.split(",")[0];
			String nodeAInterfaceLabel = line.split(",")[1];
			String nodeBLabel = line.split(",")[2];
			String nodeBInterfaceLabel = line.split(",")[3];
			
			if(nodeALabel.isEmpty() || nodeBLabel.isEmpty() || nodeAInterfaceLabel.isEmpty() || nodeBInterfaceLabel.isEmpty() ) {
				System.out.println("One or more of the input field is empty in line. Line skipped.");
				return false;
			}
		} catch(Exception e) {
			System.out.println("One or more of the input field is empty in line. Line skipped.");
			e.printStackTrace();
			return false;
		}
		return true;
	}

/*
	private static void createLayer2Connection(String nodeALabel, String nodeAInterfaceLabel, String nodeBLabel,
			String nodeBInterfaceLabel) throws Exception {

		try {
			final TopologyUpdateService topologyUpdateService = getUcmdbService().getTopologyUpdateService();
			final TopologyUpdateFactory factory = topologyUpdateService.getFactory();
			final TopologyModificationData topoModData = factory.createTopologyModificationData();

			final String name = nodeALabel + nodeBLabel;
			CI l2c = topoModData.addCI(CIT_NAME_L2C);
			l2c.setStringProperty("name", name);
			l2c.setStringProperty("display_label", name);
			
			GetInterfaceCI cmi = new GetInterfaceCI(getUcmdbService());
			
			System.out.println("Getting IF for " + nodeALabel);
			CI if1 = cmi.getInterface(nodeALabel, nodeAInterfaceLabel);
			topoModData.addCI(if1);
			
			System.out.println("Getting IF for " + nodeBLabel);
			CI if2 = cmi.getInterface(nodeBLabel, nodeBInterfaceLabel);
			topoModData.addCI(if2); 
			
			topoModData.addRelation(REL_L2C_TO_INT, l2c, if1);
			topoModData.addRelation(REL_L2C_TO_INT, l2c, if2);
			
			System.out.println("Updating Topology...");
			topologyUpdateService.create(topoModData, CreateMode.UPDATE_EXISTING);
			System.out.println("The added node ID is : " + l2c.getId());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UcmdbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	finally {
			System.out.println("Unable to communicate with UCMDB. Contact administrator.");
		}
	}
*/
	
}
