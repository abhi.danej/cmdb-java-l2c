package cadence;

import java.net.MalformedURLException;
import java.net.URL;

import com.hp.ucmdb.api.UcmdbService;
import com.hp.ucmdb.api.topology.CreateMode;
import com.hp.ucmdb.api.topology.TopologyModificationData;
import com.hp.ucmdb.api.topology.TopologyUpdateFactory;
import com.hp.ucmdb.api.topology.TopologyUpdateService;
import com.hp.ucmdb.api.types.CI;

public class LinkInterfaces {
	
    static final String CIT_NAME_HOST ="node";
    static final String CIT_NAME_L2C ="layer2_connection";
    static final String CIT_NAME_INT ="interface";
    static final String REL_L2C_TO_INT = "membership";
    
    static URL cmdbURL = null;
    static String userName = null;
    static String password = null;
    
	public static void main(String[] args) throws Exception {
		String url = args[0];
		
		
		if(validateInputs(url, args[1], args[2])) {
			userName = args[1];
			password = args[2];
			cmdbURL = new URL(url);
			linkIntefaces(cmdbURL);
		}
		System.out.println("Program Completed.");
	}
	
    private static boolean validateInputs(String url, String userName, String password) {
    	URL cmdb;
    	int port;
    	String host = null;

    	if(userName.isEmpty() || userName==null || password.isEmpty() || password==null) {
    		System.out.println("Verify the user_name and password");
    		return false;
    	}
    	
    	try {
			cmdb = new URL(url);
			host = cmdb.getHost();
			port = cmdb.getPort();
			if(port==-1) {
				System.out.println("Please specific port in URL.");
				return false;
			}
		} catch (MalformedURLException e) {
			System.out.println("Verify the URL and try again.");
			e.printStackTrace();
			return false;
		}
		return true;

	}

	public static UcmdbService getUcmdbService() throws Exception {
        return CreateSDKConnection.createSDKConnection(cmdbURL, password, password);
    }

	private static void linkIntefaces(URL cmdbURL) throws Exception {
		
        final String name = "zimbra-awscollector";
        final TopologyUpdateService topologyUpdateService = getUcmdbService().getTopologyUpdateService();
        final TopologyUpdateFactory factory = topologyUpdateService.getFactory();
        final TopologyModificationData addData = factory.createTopologyModificationData();
        
        CI l2c = addData.addCI(CIT_NAME_L2C);
        l2c.setStringProperty("name", name);
        l2c.setStringProperty("display_label", name);
        
        GetInterfaceCI cmi = new GetInterfaceCI(getUcmdbService());
        
        System.out.println("Getting zimbra if");
        CI if1 = cmi.getInterface("zimbra", "ens160");
        addData.addCI(if1);
        
        System.out.println("Getting aws if");
        CI if2 = cmi.getInterface("awscollector", "ens160");
		addData.addCI(if2); 
        
        addData.addRelation(REL_L2C_TO_INT, l2c, if1);
        addData.addRelation(REL_L2C_TO_INT, l2c, if2);
        
        System.out.println("Updating Topology...");
        topologyUpdateService.create(addData, CreateMode.UPDATE_EXISTING);
        System.out.println("The added node ID is : " + l2c.getId());

	}

}
