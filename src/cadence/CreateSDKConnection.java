package cadence;
import com.hp.ucmdb.api.*;
import java.net.MalformedURLException;
import java.net.URL;
/**
 * This is a sample of how to establish a connection to the UCMDB server.
 * This sample is connecting to the server according to the constants below.
 * WORKS ONLY WITH JAVA 8
 * 	OR YOU WILL GET NoClassDefFoundError for sun/misc/BASE64Encoder
 */
public class CreateSDKConnection {
//	public static final String PROTOCOL ="https";
//    public static final String HOST ="serviceassurance.advantageinc.org";
//    public static final int PORT = 443;
//    private static final String USER_NAME = "admin";
//    private static final String PASSWORD = "MFS0ftware%";
    
	/**
    public static final String PROTOCOL ="https";
	public static final String HOST ="serviceconfiguration.mfdemo.net";
	//	private static final String USER_NAME = "abhishekd";
    private static final String USER_NAME = "sysadmin";
    private static final String PASSWORD = "MFS0ftware%";
    public static final int PORT = 8443;
    */
    
    // Lotus OBM
//    public static final String PROTOCOL ="https";
//	public static final String HOST ="obm.innovationai.in";
//    private static final String USER_NAME = "admin";
//    private static final String PASSWORD = "Admin_123";
//    public static final int PORT = 443;
    
    /**
     * Creates a UCMDB SDK connection.
     * @param url 
     *
     * @return UcmdbService object with all of the available services
     * @throws java.net.MalformedURLException in case of a wrong host/protocol/port input
     */
    public static UcmdbService createSDKConnection(URL url, String username, String password) throws MalformedURLException {
        //Creating a service provider for a given UCMDB server address
    	String PROTOCOL = url.getProtocol();
    	String HOST = url.getHost();
    	int PORT = url.getPort();
    	String USER_NAME = username;
    	String PASSWORD = password;
        UcmdbServiceProvider serviceProvider = UcmdbServiceFactory.getServiceProvider(PROTOCOL, HOST, PORT);
        //Creating a client context according to the name of this integration (for auditing)
        ClientContext clientContext = serviceProvider.createClientContext("Layer2ConnectivityBuilder");
        //Creating the credentials for authentication
        System.out.println("CONNECTING WITH: " + USER_NAME + "@" + PASSWORD);
        Credentials credentials = serviceProvider.createCredentials(USER_NAME, PASSWORD);
        // Creating the connection object
        return serviceProvider.connect(credentials,clientContext);
     }
}