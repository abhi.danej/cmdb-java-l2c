package cadence;
import com.hp.ucmdb.api.UcmdbService;
import com.hp.ucmdb.api.topology.*;
import com.hp.ucmdb.api.types.CI;
import com.hp.ucmdb.api.types.TopologyCI;
import java.net.MalformedURLException;
import java.util.Collection;
/**
 * This is a sample of executing an AdHoc query.
 * This sample creates a query for searching nodes with at least two IPs.
 * Then, the sample prints the results.
 */
public class QueryInterfaceCI {
    public UcmdbService ucmdbService;
	private TopologyQueryService queryService;
	private TopologyQueryFactory queryFactory;

	public QueryInterfaceCI(UcmdbService ucmdbService) throws Exception {
		System.out.println("Creating connection..");
		queryService = ucmdbService.getTopologyQueryService();
		queryFactory = queryService.getFactory();
	}

public CI getInterface(String nodeDisplayLabel, String ifDisplayLabel) throws MalformedURLException {
        // Create the query definition
        System.out.println("CREATING QUERY FOR ["+nodeDisplayLabel+"]["+ifDisplayLabel+"]");
        QueryDefinition queryDefinition = queryFactory.createQueryDefinition("Get interface CI linked to a node CI.");
        //The unique name of the query node of type "node"
        
        String ifName = "Interface";

        QueryNode intQuery = queryDefinition.addNode(ifName).ofType("interface").
        		queryProperty("display_label").queryProperty("interface_macaddr").
        		property("display_label", Operator.EQUALS_CASE_INSENSITIVE, ifDisplayLabel);
        
        QueryNode nodeQuery = queryDefinition.addNode("node query").ofType("node").
        		queryProperty("display_label").
        		property("display_label",Operator.EQUALS_CASE_INSENSITIVE , nodeDisplayLabel);
        intQuery.linkedFrom(nodeQuery).withLinkOfType("composition");

        System.out.println("EXECUTING QUERY");
        Topology topology = queryService.executeQuery(queryDefinition);

        Collection<TopologyCI> ciList = topology.getCIsByName(ifName);

        TopologyCI ci = null;
        ci = ciList.iterator().next();
        System.out.println("FOUND INTERFACE CI on Node [" + nodeDisplayLabel + "]->[" + 
        		ci.getPropertyValue("display_label") + 
        		", MAC: " + ci.getPropertyValue("interface_macaddr") + "]");
        return ci;
	}
}
